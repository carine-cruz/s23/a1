//solution 3
db.rooms.insertOne(
	{
		"name": "single",
		"accomodates": 2,
		"price": 1000,
		"description": "A simple room with all the basic necessities",
		"rooms_available": 10,
		"isAvailable": false
	}
)

//solution 4
db.rooms.insertMany(
	[{
		"name": "double",
		"accomodates": 3,
		"price": 2000,
		"description": "A room fit for a small family going on a vacation",
		"rooms_available": 5,
		"isAvailable": false
	},
	{
		"name": "queen",
		"accomodates": 4,
		"price": 4000,
		"description": "A room with a queen sized bed perfect for a simple getaway",
		"rooms_available": 15,
		"isAvailable": false
	}]
)

//solution 5
db.rooms.find({"name":"double"})

//solution 6
db.rooms.updateOne(
	//filter
	{
		"_id" : ObjectId("622019c27d90ada2b518df7d")
	},
	{
		$set: {"rooms_available": 0}
	}
)

//solution 7
db.rooms.deleteMany(
	//filter
	{
		"rooms_available": { $eq: 0}
	}
)